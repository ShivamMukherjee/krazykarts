// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "GoKartMovementComponent.h"
#include "GoKartMovementReplicator.h"

#include "GoKart.generated.h"


UCLASS()
class KRAZYKARTS_API AGoKart : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGoKart();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	/**
	 * Function bound to forward axis movement
	 * @param Value - Throttle value from input controller, between -1.0 and 1.0
	 */
	void MoveForward(float Value);
	
	/**
	 * Function bound to right axis movement
	 * @param Value - Steering throw value from input controller, between -1.0 and 1.0
	 */
	void MoveRight(float Value);

	UPROPERTY(VisibleAnywhere)
	// Responsible for simulating moves on the GoKart
	UGoKartMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnywhere)
	// Responsible for proper handling of replication code and RPCs
	UGoKartMovementReplicator* MovementReplicator;
};
