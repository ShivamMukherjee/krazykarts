// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"


USTRUCT()
struct FGoKartMove
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY()
	// Keeps track of the throttle from every tick
	float Throttle;

	UPROPERTY()
	// Keeps track of the steering throw from every tick
	float SteeringThrow;

	UPROPERTY()
	// Perceived time-slices from each Tick()
	float DeltaTime;

	UPROPERTY()
	// Time when this move began
	float Time;

	FORCEINLINE bool IsValid() const
	{
		bool ValidThrottle = -1.F <= Throttle && Throttle <= 1.F;
		bool ValidSteeringThrow = -1.F <= SteeringThrow && SteeringThrow <= 1.F;

		return ValidThrottle && ValidSteeringThrow;
	}
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KRAZYKARTS_API UGoKartMovementComponent : public UActorComponent
{
	GENERATED_BODY()

	friend class AGoKart;
	friend class UGoKartMovementReplicator;

public:	
	// Sets default values for this component's properties
	UGoKartMovementComponent();
	
	// Called every frame
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	/**
	 * Creates a new move with DeltaTime, Throttle, and SteeringThrow
	 * @param DeltaTime - from each tick, in seconds
	 */
	FGoKartMove CreateMove(float DeltaTime);

	/**
	 * Simulates a GoKart's movement in advance
	 * @param Move - move to simulate in game
	 */
	void SimulateMove(const FGoKartMove& Move);

	/**
	 * Calculates an approximate air resistance that decelarates the GoKart
	 * @return air resistance in newtons
	 */
	FVector GetAirResistance();
	
	/**
	 * Calculates an approximate rolling resistance that decelerates the GoKart
	 * @return rolling resistance in newtons
	 */
	FVector GetRollingResistance();

	/**
	 * Called in Tick() to re-orient the GoKart towards the steering throw
	 * @param DeltaTime - from each tick, in seconds
	 * @param SteeringThrow - throw value generated while controls mapped to steering are pressed
	 */
	void ApplyRotation(float DeltaTime, float SteeringThrow);

	/**
	 * Called in Tick() to update the location of the GoKart
	 * @param DeltaTime - from each tick, in seconds
	 */
	void UpdateLocationFromVelocity(float DeltaTime);

	UPROPERTY(EditAnywhere)
	// Mass of the GoKart (kg)
	float Mass = 1e3F;

	UPROPERTY(EditAnywhere)
	// Force applied at full throttle (Newtons)
	float MaxDrivingForce = 10e3F;

	UPROPERTY(EditAnywhere)
	// Minimum radius of the GoKart's turning circle at full lock (m)
	float MinTurningRadius = 10;

	UPROPERTY(EditAnywhere)
	// Higher values mean more drag (kg/m). Default is 10000N / 25^2m/s = 16kg/m.
	float DragCoefficient = 16;

	UPROPERTY(EditAnywhere)
	// Higher values mean more rolling resitance. Default is maximum rolling resistance of a car on concrete (Wikipedia).
	float RollingResistanceCoefficient = 0.015F;

	// The last move as recorded by the component
	FGoKartMove LastMove;

	// The GoKart's velocity, locally maintained
	FVector Velocity;

	// Keeps track of the throttle from every tick
	float Throttle;
	
	// Keeps track of the steering throw from every tick
	float SteeringThrow;

	// Constant scaling factor to be applied for speed calculations
	static const int CM_TO_M = 100;

	// Acceleration due to gravity, here 9.8m/s^2
	float SMALL_G;
};
