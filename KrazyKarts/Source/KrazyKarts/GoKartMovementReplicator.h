// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "GoKartMovementComponent.h"

#include "GoKartMovementReplicator.generated.h"


struct FHermiteCubicSpline
{
	FORCEINLINE FVector InterpolateLocation(const float Alpha) const
	{
		return FMath::CubicInterp(StartLocation, StartDerivative, TargetLocation, TargetDerivative, Alpha);
	}

	FORCEINLINE FVector InterpolateDerivative(const float Alpha) const
	{
		return FMath::CubicInterpDerivative(StartLocation, StartDerivative, TargetLocation, TargetDerivative, Alpha);
	}

	// The starting position of the spline, P0
	FVector StartLocation;

	// The ending position of the spline, P1
	FVector TargetLocation;

	// The tangent that affects the slope of the curve close to P0, T0
	FVector StartDerivative;

	// The tangent that affects the slope of the curve close to P1, T1
	FVector TargetDerivative;
};

USTRUCT()
struct FGoKartState
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY()
	// The last move recorded by the client
	FGoKartMove LastMove;

	UPROPERTY()
	// Keeps track of the velocity of the GoKart
	FVector Velocity;

	UPROPERTY()
	// The location, rotation, and scale of the GoKart at any given moment
	FTransform Transform;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KRAZYKARTS_API UGoKartMovementReplicator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGoKartMovementReplicator();
	
	// Called every frame
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	// Scales ClientTimeBetweenUpdates for use in metres per second
	FORCEINLINE float VelocityToDerivative() const { return ClientTimeBetweenLastUpdates * CM_TO_M; }

	/**
	 * Ticks executed on the client
	 * @param DeltaTime - fractions of a second elapsed in this frame
	 */
	void ClientTick(float DeltaTime);

	/**
	 * Create a hermite cubic spline with given parameters
	 * @return Initialised spline
	 */
	FHermiteCubicSpline CreateSpline();

	/**
	 * Perform hermite cubic interpolation using given spline and alpha to set client's location
	 * @param Alpha - parameter used in cubic interpolation
	 */
	void InterpolateLocation(const FHermiteCubicSpline& Spline, const float Alpha);
	
	/**
	 * Perform hermite cubic first derivative interpolation using given spline and alpha to set client's velocity
	 * @param Alpha - parameter used in cubic interpolation
	 */
	void InterpolateVelocity(const FHermiteCubicSpline& Spline, const float Alpha);
	
	/**
	 * Perform spherical interpolation using given alpha to set client's rotation
	 * @param Alpha - parameter used in spherical interpolation
	 */
	void InterpolateRotation(const float Alpha);
	
	UFUNCTION(Server, Reliable, WithValidation)
	/**
	 * RPC for sending movement info
	 * @param Move - move to be sent to server
	 */
	void ServerSendMove(const FGoKartMove& Move);

	/**
	 * Updates the ServerState member variable with values from the owning GoKart and movement component
	 * @param Move - stored in ServerState.LastMove
	 */
	void UpdateServerState(const FGoKartMove& Move);

	/**
	 * Get rid of moves that have been acknowledged
	 * @param LastMove - last move recorded (from ServerState)
	 */
	void ClearAcknowledgedMoves(const FGoKartMove& LastMove);

	UFUNCTION()
	// Called whenever ServerState changes...
	void OnRep_ServerState();
	
	// ... on GoKarts being replicated from the server
	void OnRep_ServerState_SimulatedProxy();
	
	// ... on GoKarts controlled by clients
	void OnRep_ServerState_AutonomousProxy();

	UPROPERTY(ReplicatedUsing = OnRep_ServerState)
	// Keeps track of the GoKart's state
	FGoKartState ServerState;

	UPROPERTY()
	UGoKartMovementComponent* MovementComponent;

	UPROPERTY()
	// Separates replication of collisions from the GoKart skeletal mesh
	USceneComponent* MeshOffsetRoot;

	// A buffer of hitherto unacknowledged moves 
	// TODO this is a buffer pliss call it that thanc
	TArray<FGoKartMove> UnacknowledgedMoves;

	// Duraion since last server update
	float ClientTimeSinceUpdate;

	// Duration elapsed between previous server updates
	float ClientTimeBetweenLastUpdates;

	// Keeps track of the duration that client has simulated
	float ClientSimulatedTime;

	// Location to start cubic interpolations on client from
	FTransform ClientStartTransform;

	// Velocity to start cubic interpolations on client from
	FVector ClientStartVelocity;

	// The owning GoKart's network role (AActor::Role) as a string
	FString RoleString;

	// Constant scaling factor to be applied for speed calculations
	static const int CM_TO_M = 100;
};
